﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimpleDiaryApp.Models;

namespace SimpleDiaryApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SimpleDiaryAppController : ControllerBase
    {
        private SimpleDiaryAppContext _Context;
        public SimpleDiaryAppController(SimpleDiaryAppContext Context)
        {
            _Context = Context;
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _Context.Diary.ToArrayAsync());
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "DiaryGet")]
        public async Task<IActionResult> Get(int id)
        {
            var diary = await _Context.Diary.FirstOrDefaultAsync(_ => _.Id == id);

            if (diary == null)
            {
                return NotFound();
            }

            return Ok(diary);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]SimpleDiaryAppModels model)
        {
            try
            {
                _Context.Diary.Add(model);
                await _Context.SaveChangesAsync();
                if (model.Id != 0)
                {
                    var url = Url.Link("DiaryGet", new { id = model.Id });
                    return Created(url, model);
                }

                return BadRequest("Could not add new diary");
            }
            catch (Exception ex)
            {
                return BadRequest("Could not add new diary: " + ex.Message);
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]SimpleDiaryAppModels model)
        {
            try
            {
                var diary = await _Context.Diary.FirstOrDefaultAsync(_ => _.Id == id);

                if (diary == null)
                {
                    return NotFound();
                }

                diary.Content = model.Content;
                diary.Subtime = model.Subtime;
                diary.Name = model.Name;

                if (await _Context.SaveChangesAsync() > 0)
                {
                    return NoContent();
                }

                return BadRequest("Could not update diary");
            }
            catch (Exception ex)
            {
                return BadRequest("Could not update diary: " + ex.Message);
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var diary = await _Context.Diary.FirstOrDefaultAsync(_ => _.Id == id);

                if (diary == null)
                {
                    return NotFound();
                }

                _Context.Diary.Remove(diary);

                if (await _Context.SaveChangesAsync() > 0)
                {
                    return NoContent();
                }

                return BadRequest("Could not delete diary");
            }
            catch (Exception ex)
            {
                return BadRequest("Could not delete diary: " + ex.Message);
            }
        }
    }
}