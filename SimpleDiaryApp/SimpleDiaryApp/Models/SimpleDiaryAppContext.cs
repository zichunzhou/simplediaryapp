﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleDiaryApp.Models
{
    public class SimpleDiaryAppContext : DbContext
    {
        public SimpleDiaryAppContext(DbContextOptions<SimpleDiaryAppContext>options)
            : base(options)
        {

        }
        public DbSet<SimpleDiaryAppModels> Diary { get; set; }
       

    }
}
