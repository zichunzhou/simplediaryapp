﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleDiaryApp.Models
{
    public class SimpleDiaryAppModels
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Subtime { get; set; }
        public string Content { get; set; }
    }
}
